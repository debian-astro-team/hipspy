Source: hipspy
Section: python
Priority: optional
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-astropy (>= 1.3~),
               python3-astropy-healpix,
               python3-astropy-helpers,
               python3-numpy,
               python3-reproject (>= 0.3.1~),
               python3-setuptools,
               python3-skimage
Standards-Version: 4.5.0
Homepage: https://github.com/hipspy/hips
Vcs-Git: https://salsa.debian.org/debian-astro-team/hipspy.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/hipspy

Package: python3-hips
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Recommends: python3-aiohttp,
            python3-reproject (>=0.3.1~),
            python3-tqdm,
            pyton3-matplotlib (>=2~)
Description: Python package for Hierarchical Progressive Surveys
 HiPS (Hierarchical Progressive Surveys) is a way to store large
 astronomical survey sky image and catalog datasets on servers (such
 as HiPS at CDS), that allows clients to efficiently fetch only the
 image tiles or catalog parts for a given region of the sky they are
 interested in. Similar to Google maps, but for astronomy (see the
 HiPS paper).
 .
 This is a Python package to fetch and draw HiPS images.
